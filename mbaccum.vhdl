LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY mbaccum IS
  PORT(accin : IN std_logic_vector(7 downto 0);
       ld    : IN std_logic;
       accout : OUT std_logic_vector(7 downto 0));
END mbaccum;

ARCHITECTURE one OF mbaccum IS

BEGIN
  PROCESS
  BEGIN
    WAIT UNTIL ld='1' AND ld'event;
    accout <= accin;
  END PROCESS;
END one;

-------------------------------------------------------------------------------
--  The Test Bench Entity for testing of the accumulator
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY accumtb IS
END accumtb;

-------------------------------------------------------------------------------
--  The Test Bench Architecture -
-------------------------------------------------------------------------------
ARCHITECTURE test OF accumtb IS

  signal accin, account : std_logic_vector(7 downto 0);
  signal ld : std_logic;
  SIGNAL error : BIT := '0';

  TYPE  gval_tbl_type IS ARRAY (0 to 15) of std_logic_vector(7 downto 0);
  CONSTANT gval_tbl : gval_tbl_type :=
                ("00000000","00000001","00000010","00000011","00000100","00000101","00000110","01110000",
                 "00001000","10000001","00001010","10100001","10000100","11000001","11100000","00001111");

  COMPONENT mbaccum IS
  PORT(accin : IN std_logic_vector(7 downto 0);
       ld    : IN std_logic;
       accout : OUT std_logic_vector(7 downto 0));
END COMPONENT;

  FOR ALL : mbaccum USE ENTITY WORK.mbaccum(one);

BEGIN  --  test
  g0 : mbaccum PORT MAP(accin, ld, account);

  applytests : PROCESS
  variable num_errors : integer := 0;
  BEGIN  --  PROCESS applytests

  looper: FOR i IN 0 TO 15 LOOP
	    accin <= gval_tbl(i);
     	
	    ld <= '1', '0' AFTER 1 ns;
	   WAIT FOR 5 ns;
	IF (account /= accin)
      	THEN error <= '1', '0' AFTER 1 ns;
		num_errors := num_errors + 1;
	END IF;
	accin <= "ZZZZZZZZ";
	wait for 5 ns;
   	IF (account = accin)
     	 THEN error <= '1', '0' AFTER 1 ns;
		num_errors := num_errors + 1;
	END IF;
	   END LOOP looper;
	WAIT;
  END PROCESS applytests;

END test;