LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY mux8_2to1 IS
  PORT(minl,minr : IN std_logic_vector(7 downto 0);
       msel      : IN std_logic;
       mout      : OUT std_logic_vector(7 downto 0));
END mux8_2to1;

ARCHITECTURE one OF mux8_2to1 IS
BEGIN
  mout <= minl WHEN msel='1' ELSE minr;
END one;

-------------------------------------------------------------------------------
--  The Test Bench Entity for testing of the generic function block
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY muxtb IS
END muxtb;

-------------------------------------------------------------------------------
--  The Test Bench Architecture -
-------------------------------------------------------------------------------
ARCHITECTURE test OF muxtb IS

  signal minl, minr, mout : std_logic_vector(7 downto 0);
  signal msel : std_logic;
  SIGNAL error : BIT := '0';

  TYPE  gval_tbl_type IS ARRAY (0 to 15) of std_logic_vector(7 downto 0);
  CONSTANT gval_tbl : gval_tbl_type :=
                ("00000000","00000001","00000010","00000011","00000100","00000101","00000110","01110000",
                 "00001000","10000001","00001010","10100001","10000100","11000001","11100000","00001111");
  CONSTANT gval2_tbl : gval_tbl_type :=
                ("01100000","00011001","00100010","01100011","01010100","11000101","01100110","01111110",
                  "00001100","10011001","10001010","10111101","11100100","00000001","00100000","11111111");

  COMPONENT mux8_2to1 IS
    PORT(minl,minr : IN std_logic_vector(7 downto 0);
         msel      : IN std_logic;
         mout      : OUT std_logic_vector(7 downto 0));
  END COMPONENT;

  FOR ALL : mux8_2to1 USE ENTITY WORK.mux8_2to1(one);

BEGIN  --  test
  g0 : mux8_2to1 PORT MAP(minl, minr, msel, mout);

  applytests : PROCESS
  variable num_errors : integer := 0;
  BEGIN  --  PROCESS applytests

  loopleft: FOR i IN 0 TO 15 LOOP
	    minl <= gval_tbl(i);
      minr <= gval2_tbl(i);
	    msel <= '1';
	   WAIT FOR 25 ns;
     IF (mout /= minl)
      THEN error <= '1', '0' AFTER 1 ns;
	END IF;
	   END LOOP loopleft;

    loopright: FOR i IN 0 TO 15 LOOP
      minl <= gval_tbl(i);
      minr <= gval2_tbl(i);
      msel <= '0';
   	   WAIT FOR 25 ns;
        IF (mout /= minr )
         THEN error <= '1', '0' AFTER 1 ns;
		num_errors := num_errors + 1;
	END IF;
   	   END LOOP loopright;

	WAIT;
  END PROCESS applytests;

END test;