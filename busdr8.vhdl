LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY busdr8 IS
  PORT(din : IN std_logic_vector(7 downto 0);
       drive : IN std_logic;
       dout : OUT std_logic_vector(7 downto 0));
END busdr8;

ARCHITECTURE one OF busdr8 IS
BEGIN
  dout <= din WHEN drive='1' ELSE "ZZZZZZZZ";
END one;

-------------------------------------------------------------------------------
--  The Test Bench Entity for testing of the bus driver
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY busdrivertb IS
END busdrivertb;

-------------------------------------------------------------------------------
--  The Test Bench Architecture -
-------------------------------------------------------------------------------
ARCHITECTURE test OF busdrivertb IS

  signal din, dout : std_logic_vector(7 downto 0);
  signal drive : std_logic;
  SIGNAL error : BIT := '0';

  TYPE  gval_tbl_type IS ARRAY (0 to 15) of std_logic_vector(7 downto 0);
  CONSTANT gval_tbl : gval_tbl_type :=
                ("00000000","00000001","00000010","00000011","00000100","00000101","00000110","01110000",
                 "00001000","10000001","00001010","10100001","10000100","11000001","11100000","00001111");

  COMPONENT busdr8 IS
    PORT(din : IN std_logic_vector(7 downto 0);
         drive : IN std_logic;
         dout : OUT std_logic_vector(7 downto 0));
  END COMPONENT;

  FOR ALL : busdr8 USE ENTITY WORK.busdr8(one);

BEGIN  --  test

  g0 : busdr8 PORT MAP(din, drive, dout);
  
  applytests : PROCESS
variable num_errors : integer := 0;
  BEGIN  --  PROCESS applytests
  drivenloop: FOR i IN 0 TO 15 LOOP
	     din <= gval_tbl(i);
	    drive <= '1';
	   WAIT FOR 25 ns;
     IF (dout /= din)
      THEN error <= '1', '0' AFTER 1 ns;
	num_errors := num_errors + 1;
	END if;
	   END LOOP drivenloop;

    nodriveloop : FOR i IN 0 TO 15 LOOP
   	     din <= gval_tbl(i);
   	    drive <= '0';
   	   WAIT FOR 25 ns;
        IF (dout /= "ZZZZZZZZ" )
         THEN error <= '1', '0' AFTER 1 ns;
	num_errors := num_errors + 1;
	end if;
   	   END LOOP nodriveloop;
	WAIT;
  END PROCESS applytests;

END test;